# GIFFmpegGUI


## Description
A simple GUI application for Linux to convert videos to gifs using [FFmpeg](https://ffmpeg.org/). I can't remember all those command-line arguments!

## Disclaimer
[FFmpeg](https://ffmpeg.org/) is great software, but I have absolutely no affiliation with it. I am just a guy making something to simplify my own life and figured I would share it for anyone else who may need the same thing. That said, go support [FFmpeg](https://ffmpeg.org/)!


## TODO
- [ ] Add safety checks to ensure valid inputs (don't enter letters where numbers are expected)


## Getting started

### Instructions
- Make sure you have [FFmpeg](https://ffmpeg.org/) installed.
- Head over to the [releases](https://gitlab.com/Bitsylvania/giffmpeggui/-/releases) section to grab the most up to date version.
- Extract both GIFFmpegGUI.x86_64 and giffmpeg.sh to the same directory.
- Run GIFFmpegGUI.x86_64 and convert some videos to gifs!
