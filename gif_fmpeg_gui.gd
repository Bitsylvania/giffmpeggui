extends Node

@onready var open_file_dialog = $FileDialogOpen
@onready var save_file_dialog = $FileDialogSave
@onready var popup_error = $PopupError
@onready var error_textbox = $PopupError/PopupTextbox

# Called when the node enters the scene tree for the first time.
func _ready():
	open_file_dialog.set_current_dir("~/") # doesn't seem to work
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_convert_button_button_up():
	var _input_file = get_node("SourceTextbox").text
	var _skip_amount = get_node("SkipSecondstextbox").text
	var _output_file = get_node("OutputTextbox").text
	var _output_duration = get_node("LengthSecondsTextbox").text
	var _output_fps = get_node("AtFPSTextbox").text
	var _output_width = get_node("PixelsWideTextbox").text
	
	var _command : String = ""
	
	_command = "ffmpeg -ss " + _skip_amount
	_command += " -t " + _output_duration
	_command += " -i \"" + _input_file + "\" \\"
	_command += " -vf \"fps=" + _output_fps
	_command += ",scale=" + _output_width
	_command += ":-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse\" \\"
	_command += " -loop 0 \"" + _output_file + "\""
	
	
	var output = []
	#var exit_code = OS.execute("ffmpeg", ["-ss", "--help"], output)
	var exit_code = OS.execute("bash", ["-c", OS.get_executable_path().get_base_dir() + "/giffmpeg.sh " + _input_file + " " + _skip_amount + " " + _output_file + " " + _output_duration + " " + _output_fps + " " + _output_width], output)
	if exit_code != 0:
		popup_error.title = "ERROR! EXIT CODE " + str(exit_code)
		match exit_code:
			1:
				error_textbox.text = "File not found!"
			_:
				error_textbox.text = ""
				for msg in output.size():
					error_textbox.text += str(output[msg])
		popup_error.show()
	else:
		popup_error.title = "SUCCESS!"
		error_textbox.text = "Video to gif conversion is complete!"
		popup_error.show()


func _on_source_button_button_up():
	open_file_dialog.show()
	pass # Replace with function body.


func _on_file_dialog_open_file_selected(path):
	get_node("SourceTextbox").text = path
	pass # Replace with function body.


func _on_output_button_button_up():
	save_file_dialog.show()
	pass # Replace with function body.


func _on_file_dialog_save_file_selected(path):
	get_node("OutputTextbox").text = path
	pass # Replace with function body.
